package com.wevioo.reconnaissanceBack.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
 @NoArgsConstructor
@Builder
public class Client implements Serializable {
    @Id
    private long  cin;
    private String nom;
    private String  prenom;
    private  String dateNaissance;
    private byte[] signature;
}
