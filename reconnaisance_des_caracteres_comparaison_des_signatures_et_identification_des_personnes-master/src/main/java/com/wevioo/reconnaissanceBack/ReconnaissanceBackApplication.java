package com.wevioo.reconnaissanceBack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReconnaissanceBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReconnaissanceBackApplication.class, args);
	}

}
