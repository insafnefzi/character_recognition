package com.wevioo.reconnaissanceBack.controllers;

import com.wevioo.reconnaissanceBack.entities.Client;
import com.wevioo.reconnaissanceBack.repositories.Iclient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/clients")
@CrossOrigin(origins = "*", maxAge = 3600)

public class ClientController  {
    @Autowired
    private Iclient clientRepository;
    @GetMapping("/")
    @CrossOrigin(allowedHeaders = "*")
   public List<Client>findAll(){
        return clientRepository.findAll();
    }

    @PostMapping("/")
    public ResponseEntity createClient(@RequestBody Client client){
        if(client ==null){
            return ResponseEntity.badRequest().body("cannot create an employee with empty fields");
        }
        Client createdClient=clientRepository.save(client);


        return ResponseEntity.ok(createdClient);
    }
    @DeleteMapping("/{cin}")
    public  ResponseEntity deleteClient(@PathVariable(name = "cin")Long cin){
        if(cin==null){
            return  ResponseEntity.badRequest().body("cannot remove character with null cin ");
        }
        Client client =clientRepository.getOne(cin);
        if(client==null){
            return ResponseEntity.notFound().build();
        }
        clientRepository.delete(client);
        return ResponseEntity.ok("client removed");
    }


}
