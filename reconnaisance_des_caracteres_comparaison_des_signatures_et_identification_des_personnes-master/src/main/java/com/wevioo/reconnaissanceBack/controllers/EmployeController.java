package com.wevioo.reconnaissanceBack.controllers;

import com.wevioo.reconnaissanceBack.entities.Employe;
import com.wevioo.reconnaissanceBack.repositories.IEmploye;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1/employes")
public class EmployeController {
    @Autowired
    private IEmploye employeRepository;
@GetMapping("/")
    public ResponseEntity findAll(){
    return  ResponseEntity.ok(employeRepository.findAll());
}
@PostMapping("/")
    public ResponseEntity createEmploye(@RequestBody Employe employe){
    if (employe==null){
        return ResponseEntity.badRequest().body("cannot create employe with empty fields");
    }
Employe createdEmploye=employeRepository.save(employe);
    return ResponseEntity.ok(createdEmploye);
}
@PostMapping("/login")
    public ResponseEntity login(@RequestParam(name = "matricule")String matricule,@RequestParam(name = "password") String password){
    if (StringUtils.isEmpty(matricule)||(StringUtils.isEmpty(password))) {
        ResponseEntity.badRequest().body("cannot login with empty employe");
    }
    Employe authenticatedEmploye=employeRepository.findByMatriculeAndPassword(matricule, password);
    if(authenticatedEmploye==null){
        return ResponseEntity.notFound().build();//comment
    }
    return ResponseEntity.ok(authenticatedEmploye);
}
@GetMapping("/find")
public Employe findEmpolye(@RequestParam(name = "matricule")String matricule,@RequestParam(name = "password") String password){
    return  employeRepository.findByMatriculeAndPassword(matricule, password);
}

}
