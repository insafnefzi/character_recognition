package com.wevioo.reconnaissanceBack.repositories;

import com.wevioo.reconnaissanceBack.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Iclient extends JpaRepository<Client,Long> {
    List<Client>findAll();
}
