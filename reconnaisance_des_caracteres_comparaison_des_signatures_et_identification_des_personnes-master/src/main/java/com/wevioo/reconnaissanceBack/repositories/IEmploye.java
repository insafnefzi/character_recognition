package com.wevioo.reconnaissanceBack.repositories;

import com.wevioo.reconnaissanceBack.entities.Employe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmploye extends JpaRepository<Employe,Long> {
    Employe findByMatriculeAndPassword(String matricule, String password);
}
